﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public float timer=7;

    public  List<GameObject> enemies;

    public GameObject instantiatePos;

    private float time = 0;

    private int multiplier = 20;

    void Start()
    {
        
    }

    void Update()
    {
        timer -= Time.deltaTime;
        SpawnEnemies();
        ChangeVelocity();
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        if (time > multiplier)
        {
            multiplier *= 2;
            //Increase velocity
        }
    }

    private void SpawnEnemies()
    {
        if (timer <= 0)
        {
            float offsetX = instantiatePos.transform.position.x;
            int rnd = UnityEngine.Random.Range(0, enemies.Count);
            for (int i = 0; i < 5; i++)
            {
                if (rnd == 3) //Comparacion para spawnear de forma distinta a enemigos comunes y boss
                {
                    //TODO arreglar la posicion de spawn del boss para que no se superponga con otros enemigos
                    offsetX = offsetX - 4;
                    float offsetY = instantiatePos.transform.position.y - 8;
                    for (int j = 0; j < 4; j++)
                    {
                        offsetY = offsetY + 4;
                        Vector3 transform = new Vector3(offsetX, offsetY, instantiatePos.transform.position.z);
                        Instantiate(enemies[rnd], transform, Quaternion.identity);
                    }
                }
                else if (rnd != 3)
                {
                    offsetX = offsetX + 4;
                    Vector3 transform = new Vector3(offsetX, instantiatePos.transform.position.y, instantiatePos.transform.position.z);
                    Instantiate(enemies[rnd], transform, Quaternion.identity);
                }

                
            }
            timer = 7;
        }
    }
}
