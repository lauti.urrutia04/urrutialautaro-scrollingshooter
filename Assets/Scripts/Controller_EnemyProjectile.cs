﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_EnemyProjectile : Projectile //heredar de la clase projectile
{
    private GameObject player;

    private Vector3 direction;

    private Rigidbody rb;

    public float enemyProjectileSpeed;

    void Start()
    {
        if (Controller_Player._Player != null)
        {
            //buscar y asignar el gameObject player
            player = Controller_Player._Player.gameObject;
            direction = -(this.transform.localPosition - player.transform.localPosition).normalized;
        }
        rb = GetComponent<Rigidbody>();
    }

    
    public override void Update()
    {
        //al estar en update, los proyectiles reciben el impulso constantemente y aceleran
        rb.AddForce(direction*enemyProjectileSpeed, ForceMode.Force);
        Destroy(this.gameObject, 5);
        base.Update();
    }
}
