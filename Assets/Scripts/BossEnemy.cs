using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEnemy : Controller_Enemy
{
    private Rigidbody rb;

    public int baseHp = 5;
    private int hp;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        hp = baseHp;
    }
    
    private void FixedUpdate()
    {
        if (hp <= 0) //habia escrito un override Update() pero lo elimino porque no tiene sentido
        {
            Destroy(this.gameObject);
        }
        rb.AddForce(new Vector3(-1, 0, 0) * enemySpeed);
    }

    internal override void OnCollisionEnter(Collision collision) //sobreescribo el collision del boss para que no muera de 1 impacto
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            hp -= 1;
            Destroy(collision.gameObject);
            Controller_Hud.points++;
        }
        if (collision.gameObject.CompareTag("Laser"))
        {
            hp -= 2;
            Destroy(collision.gameObject);
            Controller_Hud.points++;
        }
    }
}
