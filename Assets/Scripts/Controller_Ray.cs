using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Ray : Projectile
{
    public float raySpeed;
    public Rigidbody rb;
    public GameObject player;
    internal int uses;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        uses = 15;
    }


    private void FixedUpdate()
    {
        transform.position = new Vector3 (player.transform.position.x + 11, player.transform.position.y, 0);
    }

    internal override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            uses--;
            if (uses < 1)
            {
                Destroy(this.gameObject);
            }
        }
        base.OnCollisionEnter(collision);
    }
}
